/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        xxs: "375px",
        xs: "480px",
        sm: "577px",
        small: "769px",
        md: "993px",
        tablet: "1024px",
        lg: "1080px",
        1200: "1200px",
        xl: "1280px",
        "2xl": "1920px",
      },
      colors: {
        primary: "#000000",
        secondary: "#737373",
        white: "#ffffff",
      },
    },
  },
  plugins: [],
};
