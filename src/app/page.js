import React from "react";
import HeroContainer from "./containers/Index/Hero/HeroContainer";
import Platform from "./containers/Index/Platform/Platform";
import Categories from "./containers/Index/Categories/Categories";
import Feature from "./containers/Index/Feature/Feature";
import Brands from "./containers/Index/Brands/Brands";

export default function Home() {
  return (
    <section>
      <HeroContainer />
      <Platform />
      <Categories />
      <Feature />
      <Brands />
    </section>
  );
}
