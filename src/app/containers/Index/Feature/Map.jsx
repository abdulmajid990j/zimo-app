import React from "react";

const Map = () => {
  return (
    <div className="map-container bg-[url('/map.png')] bg-cover bg-center bg-no-repeat py-[200px]">
      <div className="title flex flex-col items-center justify-center">
        <h3 className="text-[25px] text-primary uppercase tracking-[5px]">
          global vision
        </h3>
        <p className="text-[12px] text-primary tracking-[3px] mt-7 text-center">
          WITH A REMOTE CULTURE , DIVERSITY IS NATURALLY IN OUR DNA.
        </p>
        <p className="text-[12px] text-primary tracking-[3px] mt-2 text-center">
          BASED ACROSS THE GLOBE, MAKING UP OVER 23 DIFFERENT NATIONALITIES.
        </p>
      </div>
    </div>
  );
};

export default Map;
