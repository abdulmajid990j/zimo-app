import React from "react";
import BoatBanner from "./BoatBanner";
import Map from "./Map";
import "../../../common/styles/custom.css";

const Feature = () => {
  return (
    <>
      <div className="boat-container bg-[url('/boat-banner.png')] bg-cover sm:px-10 px-5 py-10">
        <BoatBanner />
      </div>
      <div className="world-map-image sm:px-10 px-5 py-10 relative border-b-[1px] border-primary">
        <Map />
      </div>
    </>
  );
};

export default Feature;
