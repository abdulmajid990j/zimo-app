import React from "react";
import Image from "next/image";
// impor images
import arrow from "../../../common/images/arrow.png";

const BoatBanner = () => {
  return (
    <div className="banner-container py-5">
      <div className="content flex flex-col md:items-end items-start justify-start">
        <h3 className="md:text-[30px] text-[22px] text-white uppercase tracking-[5px] mb-5">
          personalised for you
        </h3>
        <p className="text-[13px] text-white tracking-[3px] lg:w-[38%] sm:w-[45%] w-full leading-[30px]">
          DELIVERING THOUSANDS OF PERSONALISED ALERTS EVERYDAY, OUR USERS CAN BE
          FIRST IN LINE WHEN THAT OPPORTUNITY OF A LIFETIME IS HERE.
        </p>
      </div>
      <div className="btn-container flex items-center justify-end pt-[150px] pb-[200px]">
        <button className="create-user text-[14px] text-white uppercase flex items-center tracking-[3px]">
          create user id
          <Image src={arrow} alt="arrow image" className="w-[33px] ml-2" />
        </button>
      </div>
    </div>
  );
};

export default BoatBanner;
