import React from "react";
import Image from "next/image";
import { BsChevronDown } from "react-icons/bs";

//import images
import blackLogo from "../../../common/images/black-logo.png";
import blackChevron from "../../../common/images/black-chevron-down.png";

const Platform = () => {
  return (
    <div className="platform-container max-w-[1920px] mx-auto sm:px-10 px-5 py-10 md:text-left text-center">
      <div className="light-description text-center">
        <p className="text-[13px] text-primary tracking-[3px] uppercase">
          a real estate and property platform that is changing the world
        </p>
      </div>
      <div className="titles pt-[60px]">
        <h3 className="md:text-[28px] sm:text-[22px] text-[18px] text-primary font-normal tracking-[3px] uppercase mb-1">
          a revolutionary platform
        </h3>
        <h2 className="md:text-[47px] sm:text-[37px] text-[30px] text-primary font-normal tracking-[3px] uppercase md:leading-[50px] leading-[40px]">
          enteries - sellers
        </h2>
        <h1 className="md:text-[70px] sm:text-[55px] text-[45px] text-primary font-normal tracking-[5px] uppercase md:leading-[85px] leading-[75px]">
          worldwide
        </h1>
      </div>
      <div className="horizontal-right-content flex flex-col md:items-end items-center md:justify-end justify-center pb-5 md:my-0 my-10">
        <Image
          src={blackLogo}
          alt="black logo"
          className="md:w-[40%] w-[30%]"
        />
        <div className="description">
          <p className="text-[14px] text-primary uppercase md:w-[45%] w-full text-center tracking-[3px] leading-[30px] mt-7 md:ml-auto md:mx-0 mx-aut">
            connecting user from users from across the globe to facilitate
            life&lsquo;s most important personal transactions
          </p>
        </div>
      </div>
      <div className="best-content">
        <h3 className="md:text-[28px] text-[20px] text-primary font-normal tracking-[3px] uppercase">
          the best of the best
        </h3>
        <p className="text-[13px] text-primary md:w-[65%] w-full md:mr-auto md:mx-0 mx-auto tracking-[3px] leading-[30px] mt-7">
          A COMBINATION OF AUTOMATION AND MANUAL CURATION OUR PRO AGENTS AND
          MODERATION TEAM SELECTS THE HIGHEST QUALITY LISTING S ON THE MARKET
          FROM ACROSS THE WORLD.
        </p>
        <div className="chevron flex justify-center pt-5">
          <Image
            src={blackChevron}
            alt="black chevron"
            className="md:w-[60px] w-[40px]"
          />
        </div>
      </div>
    </div>
  );
};

export default Platform;
