import React from "react";
import Image from "next/image";
import "../../../common/styles/custom.css";

// import images
import blackZimo from "../../../common/images/black-zimo-logo.png";
import property from "../../../../../public/property.jpg";

const Tickets = () => {
  return (
    <div className="tickets-container">
      <div className="seperate-title text-center mt-20">
        <h2 className="md:text-[35px] sm:text-[27px] text-[22px] text-primary tracking-[2px] uppercase mb-7">
          one platform for all premium listings
        </h2>
        <h3 className="md:text-[26px] text-[20px] text-[#333] tracking-[3px] uppercase">
          unlimited potential
        </h3>
      </div>
      <div className="entry-container flex md:flex-row flex-col-reverse items-center justify-between mt-20">
        <div className="content flex-[1.1_1_0%] md:mr-[60px] mr-0 flex flex-col md:items-start items-center justify-start">
          <Image
            src={blackZimo}
            alt="black zimo logo"
            className="md:w-full w-1/2"
          />
          <p className="md:text-[26px] text-[20px] text-primary uppercase tracking-[3px] mt-5">
            one source
          </p>
          <h4 className="md:text-[40px] text-[30px] text-primary uppercase tracking-[3px]">
            entry tickets
          </h4>
          <p className="text-[14px] text-secondary uppercase tracking-[3px] mt-3">
            for all premium listings
          </p>
        </div>
        <div className="image flex-[2_1_0%] h-[500px] relative md:mb-0 mb-10">
          <Image
            src={property}
            alt="property image"
            className="rounded-[20px]"
          />
        </div>
      </div>
    </div>
  );
};

export default Tickets;
