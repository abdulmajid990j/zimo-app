import React from "react";
import Carousel from "./Carousel";
import Tickets from "./Tickets";

const Categories = () => {
  return (
    <div className="category-container max-w-[1920px] sm:px-10 px-5 py-10">
      <Carousel />
      <Tickets />
    </div>
  );
};

export default Categories;
