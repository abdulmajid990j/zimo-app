import React from "react";
import Image from "next/image";

//import images
import ChevronLeft from "../../../common/images/chevron-left.png";
import ChevronRight from "../../../common/images/chevron-right.png";

const Carousel = () => {
  return (
    <div className="carousel-container">
      <div className="title flex items-center justify-between">
        <h3 className="md:text-[28px] text-[20px] text-primary font-normal tracking-[3px] uppercase">
          our categories
        </h3>
        <div className="btn-container">
          <button className="prev-btn md:px-5 px-3">
            <Image
              src={ChevronLeft}
              alt="chevron left"
              className="md:w-[16px] w-[10px]"
            />
          </button>
          <button className="next-btn md:px-5 px-3">
            <Image
              src={ChevronRight}
              alt="chevron right"
              className="md:w-[16px] w-[10px]"
            />
          </button>
        </div>
      </div>
      <div className="carousel-items grid md:grid-cols-4 small:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-x-[30px] pt-7">
        <div className="real-estate bg-[url('/estate.jpg')] bg-center bg-cover w-full h-[280px] rounded-[20px] flex items-end justify-center p-5 md:mb-0 mb-5">
          <p className="text-[15px] text-white font-normal uppercase tracking-[3px]">
            real estate
          </p>
        </div>
        <div className="cars bg-[url('/cars.png')] bg-center bg-cover w-full h-[280px] rounded-[20px] flex items-end justify-center p-5 md:mb-0 mb-5">
          <p className="text-[15px] text-white font-normal uppercase tracking-[3px]">
            cars
          </p>
        </div>
        <div className="yachts bg-[url('/yachts.png')] bg-center bg-cover w-full h-[280px] rounded-[20px] flex items-end justify-center p-5 md:mb-0 mb-5">
          <p className="text-[15px] text-white font-normal uppercase tracking-[3px]">
            yachts
          </p>
        </div>
        <div className="watches bg-[url('/watch.png')] bg-center bg-cover w-full h-[280px] rounded-[20px] flex items-end justify-center p-5 md:mb-0 mb-5">
          <p className="text-[15px] text-white font-normal uppercase tracking-[3px]">
            watches
          </p>
        </div>
      </div>
    </div>
  );
};

export default Carousel;
