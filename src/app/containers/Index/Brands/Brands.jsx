import React from "react";
import Image from "next/image";

// import images
import blackZimo from "../../../common/images/black-zimo-logo.png";
import brandImg from "../../../common/images/brand-image.png";
import transparentLogo from "../../../common/images/transparent-logo.png";
import car from "../../../common/images/car.jpg";

const Brands = () => {
  return (
    <div className="brands-container max-w-[1920px]">
      <div className="flex flex-col justify-center sm:h-[700px] h-full sm:my-0 my-10">
        <div className="brand-wrapper grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 sm:px-20 px-5">
          <div className="">
            <Image
              src={blackZimo}
              alt="black zimo image"
              className="sm:w-[210px] w-[170px]"
            />
            <h3 className="sm:text-[40px] text-[30px] text-secondary tracking-[3px] uppercase">
              partners
            </h3>
          </div>
          <div className="sm:mt-0 mt-10">
            <Image
              src={brandImg}
              alt="brand image"
              className="sm:w-[210px] w-[170px]"
            />
          </div>
        </div>
      </div>
      <div className="boat-banner bg-[url('/boat-banner-2.jpg')] bg-cover py-[300px]"></div>
      <div className="images flex sm:px-20 px-5">
        <div className="relative top-[-240px]">
          <Image
            src={car}
            alt="car image"
            className="w-[350px] h-[500px] rounded-[20px]"
          />
        </div>
        <div className="w-[47%] md:block hidden relative top-[-80px] ml-auto">
          <Image
            src={transparentLogo}
            alt="transparent logo"
            className="w-full"
          />
        </div>
      </div>
    </div>
  );
};

export default Brands;
