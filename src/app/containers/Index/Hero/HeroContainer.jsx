import React from "react";
import TopHeader from "./TopHeader";
import Image from "next/image";
import { BsArrowLeftShort } from "react-icons/bs";

// import images
import whiteChevron from "../../../common/images/white-chevron-down.png";

const HeroContainer = () => {
  return (
    <div className="hero-banner bg-[url('/banner.png')] bg-cover max-w-[1920px] mx-auto sm:px-10 px-5">
      <TopHeader />
      <div className="btn-container pb-5">
        <button className="text-[14px] text-white uppercase flex items-center">
          <BsArrowLeftShort className="text-2xl text-white mr-1" />
          back
        </button>
      </div>
      <div className="vertical-intro py-[120px]">
        <h1 className="md:text-2xl text-[18px] text-white mb-3 tracking-[2px]">
          DISCOVER
        </h1>
        <h2 className="md:text-[35px] text-[25px] text-white mb-3 tracking-[2px]">
          A NEW WORLD
        </h2>
        <p className="text-[14px] text-secondary tracking-[2px]">
          FOR THOSE WHO WISH FOR MORE...
        </p>
      </div>
      <div className="bring-content flex flex-col items-center justify-center">
        <h4 className="text-[14px] text-secondary tracking-[2px] text-center font-normal mb-5">
          BRINGING THE WORLD CLOSER TOGETHER
        </h4>
        <Image
          src={whiteChevron}
          alt="black chevron"
          className="md:w-[60px] w-[40px] mb-3"
        />
      </div>
    </div>
  );
};

export default HeroContainer;
