import React from "react";
import Image from "next/image";
import Link from "next/link";

// import images
import smallLogo from "../../../common/images/header-small-logo.png";
import zimoLogo from "../../../common/images/zimo-logo.png";
import headLogo from "../../../common/images/header-logo.png";
import flag from "../../../common/images/uk-flag.png";
import user from "../../../common/images/user-icon.png";
import bag from "../../../common/images/shopping-bag.png";

const TopHeader = () => {
  return (
    <div className="header-container py-7 flex items-center justify-between">
      <div className="logos-wrapper flex-1 flex items-end">
        <Link href="/">
          <Image
            src={smallLogo}
            alt="small header logo"
            className="sm:w-[50px] w-[40px] sm:h-[30px] h-[18px] mr-3"
          />
        </Link>
        <Link href="/">
          <Image
            src={zimoLogo}
            alt="zimo logo"
            className="sm:w-[100px] w-[65px]"
          />
        </Link>
        <p className="text-[12px] text-white uppercase ml-3 mb-0">About</p>
      </div>
      <div className="head-logo flex-1 md:flex hidden justify-center">
        <Link href="/">
          <Image src={headLogo} alt="header logo" className="w-[110px]" />
        </Link>
      </div>
      <div className="details-bar flex-1 flex items-center small:justify-evenly justify-end">
        <div className="flag-wrapper small:flex hidden items-center">
          <div className="content mr-3">
            <p className="text-[10px] uppercase text-white">
              17:23 <span className="mx-2">london</span> united kingdom
            </p>
            <p className="text-[10px] uppercase text-[#937c48] text-right">
              sunday, 12 feburary 2023
            </p>
          </div>
          <Image src={flag} alt="country flag" className="w-[30px]" />
        </div>
        <div className="shopping-bag mx-5">
          <Link href="/">
            <Image src={bag} alt="header logo" className="w-[15px]" />
          </Link>
        </div>
        <div className="user-icon">
          <Link href="/">
            <Image src={user} alt="header logo" className="w-[15px]" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default TopHeader;
